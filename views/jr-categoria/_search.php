<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JrCardapioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jr-cardapio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'data') ?>

    <?= $form->field($model, 'Salada') ?>

    <?= $form->field($model, 'Pratoprincipal') ?>

    <?= $form->field($model, 'Carne') ?>

    <?php // echo $form->field($model, 'Sobremesa') ?>

    <?php // echo $form->field($model, 'Suco') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
