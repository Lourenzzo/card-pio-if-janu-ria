<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JrCardapio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jr-cardapio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'data')->textInput() ?>

    <?= $form->field($model, 'Salada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Pratoprincipal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Carne')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Sobremesa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Suco')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
