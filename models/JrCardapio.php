<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jr_cardapio".
 *
 * @property int $id
 * @property string $data
 * @property string $Salada
 * @property string $Pratoprincipal
 * @property string $Carne
 * @property string $Sobremesa
 * @property string $Suco
 */
class JrCardapio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jr_cardapio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['Salada', 'Carne', 'Sobremesa', 'Suco'], 'string', 'max' => 20],
            [['Pratoprincipal'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'Salada' => 'Salada',
            'Pratoprincipal' => 'Pratoprincipal',
            'Carne' => 'Carne',
            'Sobremesa' => 'Sobremesa',
            'Suco' => 'Suco',
        ];
    }
}
